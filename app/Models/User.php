<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Uuid;

class User extends Model
{
    use SoftDeletes, Uuid;

    protected $fillable = [
        'uuid',
        'name',
        'email'
    ];

    protected $dates = ['deleted_at'];
}
